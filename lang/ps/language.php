<?php

return [

    'Resident' => 'اوسیدونکی',
    'Shahrak' => 'ښارګوټی',
    'type' => 'ډول',
    'ApartmentNo' => ' د اپارتمان شمیره',
    'Tipe' => 'تیپ',
    'Location' => 'ځای',
    'Room' => 'کوټه',
    'Hall' => 'تالار',
    'Bathroom' => 'تشناب',
    'kitchen' => 'پخلنځي',
    'Floor' => 'منزل',
    'Block' => 'بلاک',
    'East' => 'ختیځ',
    'West' => 'لویدیځ',
    'North' => 'شمال',
    'South' => 'سویل',
    'Areasize' => 'مساحت',
    'Totalprice' => ' ټول قیمت',
    'percentage' => 'سلنه',
    'NetAmount' => 'خالص مقدار',
    'Date' => 'تاریح ',
    'submit' => 'ثبت',
    // This is related land pages
    'Land' => 'ځمکی شمیره',
    'District' => 'ناحیه',
    // This is related Resident page
    'Name' => 'نوم',
    'FatherName' => 'دپلار نوم',
    'GrandfatherName' => 'دنیکه نوم',
    'NIC' => 'تذکره',
    'Book' => 'جلد',
    'Page' => 'صفحه',
    'province' => 'ولایت',
    'Village' => 'کلی',
    // This is related Paments page Apartment
    'Apartment' => 'اپارتمان',
    'AwizNo' => 'د اویز شمیره',
    'PartialNo' => 'دپارسل شمیره',
    'Amount' => 'مقدار',
    'Land' => 'ځمکه',
    //this is used in shahrak District text box loop
    'Districtlist' => 'لیست ',
];