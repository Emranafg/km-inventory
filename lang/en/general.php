<?php

return [

    'apartment_list' => 'لیست آپارتمان',
    'Shahrak' => 'شهرک',
    'type' => 'نوع',
    'ApartmentNo' => 'شماره آپارتمان',
    'Tipe' => 'تیپ',
    'Location' => 'موقعیت',
    'Room' => 'اطاق',
    'Hall' => 'صالون',
    'Bathroom' => 'تشناب',
    'kitchen' => 'آشپزخانه',
    'Floor' => 'منزل',
    'Block' => 'بلاک',
    'East' => 'شرق',
    'West' => 'غرب',
    'North' => 'شمال',
    'South' => 'جنوب',
    'Areasize' => 'مساحت',
    'Totalprice' => 'قیمت مجموعی',
    'percentage' => 'فیصد',
    'NetAmount' => 'مقدار خالص',
    'Date' => 'تاریخ',
    'submit' => 'ثبت',
    // This is related land page
    'Land' => 'شماره زمین',
    'District' => 'ناحیه',
    // This is related Resident page
    'Name' => 'نام',
    'FatherName' => 'نام پدر',
    'GrandfatherName' => 'نام پدر بزرگ',
    'NIC' => 'تذکره',
    'Book' => 'جلد',
    'Page' => 'صفحه',
    'province' => 'ولایت',
    'Village' => 'قریه',
    // This is related Paments page Apartment
    'Apartment' => 'اپارتمان',
    'AwizNo' => 'شماره اویز',
    'PartialNo' => 'شماره پارسل',
    'Amount' => 'مقدار',
    'Land' => 'زمین',
    //this is used in shahrak District text box loop
    'Districtlist' => 'District list ',



];
