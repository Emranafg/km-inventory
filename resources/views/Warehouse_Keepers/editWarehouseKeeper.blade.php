
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Warehouse Update Form</span>
            <span class="caption-helper">Please Update Warehouse Keeper info if not Correctly inserted in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        @if (Session::has('warehouseKeeperUpdate'))

        <div class="alert alert-success" role="alert">
            {{Session::get('warehouseKeeperUpdate')}}
        </div>
        
    @endif
        <!-- BEGIN FORM-->
        <form action="{{route('UpdatWarehousKeeper.Update')}}" method="POST" class="form-horizontal">
            @csrf

            <input type="hidden" name="id" value="{{$warehouse_keeper->id}}">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="name" value="{{$warehouse_keeper->name}}" id="name" placeholder="Enter Name of Employee">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                
            </div>

            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Last Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="last_name" value="{{$warehouse_keeper->last_name}}" id="name" placeholder="nter Last Name of Warehouse keeper">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
    
            </div>


            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Phone</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="phone" value="{{$warehouse_keeper->phone}}" id="name" placeholder="nter phone of Warehouse keeper">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
    
            </div>


            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Email</label>
                    <div class="col-md-8">
                        <input  type="email" class="form-control" name="email" value="{{$warehouse_keeper->email}}" id="name" placeholder="Enter Email of Warehouse keeper">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
    
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection