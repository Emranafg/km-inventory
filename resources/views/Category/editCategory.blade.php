
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Category Update Form</span>
            <span class="caption-helper">Please Update Item Category if not Correctly inserted in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        @if (Session::has('updateCategory'))
        <div class="alert alert-success" role="alert">
            {{Session::get('updateCategory')}}
        </div>
        
    @endif
        <form action="{{route('UpdateCategory.Update')}}" method="POST" class="form-horizontal">
            @csrf
            <input type="hidden" name="id" value="{{$category->id}}">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Category Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="name" value="{{$category->name}}" id="name" placeholder="Enter Name of Employee">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection