
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Update Item Type Form</span>
            <span class="caption-helper">Please Upadate Item type if inaccuratly inserted in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('ItemType.Update')}}" method="POST" class="form-horizontal">
            @if (Session::has('itemTypeUpdate',))
                <div class="alert alert-success" role="alert">
                    {{session::get('itemTypeUpdate')}}
                </div>
            @endif

            @csrf
            <input type="hidden" name="id" value="{{$itemType->id}}" />
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Type</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="name" value="{{$itemType->name}}" id="name" placeholder="Enter Name of Employee">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Update</button>
                        <button type="reset" value="reset" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection