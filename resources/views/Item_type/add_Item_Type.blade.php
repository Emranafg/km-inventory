
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Item Type Enrty Form</span>
            <span class="caption-helper">Please Add Item type if not Existed in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('itemType.store')}}" method="POST" class="form-horizontal">
            @if (Session::has('add_Item_Type',))
                <div class="alert alert-success" role="alert">
                    {{session::get('add_Item_Type')}}
                </div>
                
            @endif
            @csrf
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Type</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="name" value="" id="name" placeholder="Enter Name of Employee">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" value="reset" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection