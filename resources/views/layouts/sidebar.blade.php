            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <form class="sidebar-search  sidebar-search-bordered" action="http://keenthemes.com/preview/metronic/theme/admin_1_rounded/page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_2.html" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Dashboard 2</span>
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_3.html" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Dashboard 3</span>
                                            <span class="badge badge-danger">5</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">UI Features</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="ui_colors.html" class="nav-link ">
                                            <span class="title">Color Library</span>
                                        </a>
                                    </li>

                                    <li class="nav-item  ">
                                        <a href="ui_colors.html" class="nav-link ">
                                            <span class="title">Color Library</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <span class="title">Page Progress Bar</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="ui_page_progress_style_1.html" class="nav-link "> Flash </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="ui_page_progress_style_2.html" class="nav-link "> Big Counter </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Items</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('addItem.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Item</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('showItem.show')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span class="title">Item List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                        

                           
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Items Type</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('itemType.add')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Item type</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('listItemType.show')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span class="title">Item Type List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                        
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Items Status</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('addItemStatus.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Item Status</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('showItemStatus.show')}}" class="nav-link nav-toggle">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                            <span class="title">Item Statu List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                            {{-- End Items status --}}

                            {{-- Start Employee Section  --}}
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Employee</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('addEmployee.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Employee</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('showEmployee.show')}}" class="nav-link nav-toggle">
                                            <span><i class="fa fa-list" aria-hidden="true"></i></span>
                                            <span class="title">Employee List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>

                            {{-- Start Category Section --}}
                            <li class="nav-item  ">
                                <a href="" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Category</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('addCategory.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Category</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('listCategory.show')}}" class="nav-link nav-toggle">
                                            <span><i class="fa fa-list" aria-hidden="true"></i></span>
                                            <span class="title">Category List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>

                            {{-- Start Department Section--}}
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Department</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('addDepartment.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Department</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item  ">
                                        <a href="{{route('showDepartment.show')}}" class="nav-link nav-toggle">
                                            <span><i class="fa fa-list" aria-hidden="true"></i></span>
                                            <span class="title">Department List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                            {{-- Start Unit Section --}}
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa-regular fa-gear"></i>
                                    <span class="title">Item Unit</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{route('addUnit.create')}}" class="nav-link ">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Add Item Unit</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a href="{{route('showUnit.show')}}" class="nav-link nav-toggle">
                                            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            <span class="title">Item Unit List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                            {{--Start Donor Section --}}
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <span class="fa fa-duotone"></span>
                                    <span class="title">Donor|Supplier</span>

                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{route('addDonor.create')}}" class="nav-link ">
                                            <span class="fa fa-plus-circle" ></span>
                                            <span class="title">Add Donor</span>
                                            
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a href="{{route('showDoonar.show')}}" class="nav-link nav-toggle">
                                            <span class="fa fa-list" ></span>
                                            <span class="title">List Donor</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>

                            {{--Start Donor Section --}}
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <span class="fa fa-duotone"></span>
                                    <span class="title">Warehouse Keepers</span>

                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{route('addWarehouseKeeper.create')}}" class="nav-link ">
                                            <span class="fa fa-plus-circle" ></span>
                                            <span class="title">Add </span>
                                            
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a href="{{route('showWarehouseKeepers.show')}}" class="nav-link nav-toggle">
                                            <span class="fa fa-list" ></span>
                                            <span class="title">List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <span class="fa fa-duotone"></span>
                                    <span class="title">Users</span>

                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{route('users.create')}}" class="nav-link ">
                                            <span class="fa fa-plus-circle" ></span>
                                            <span class="title">Add </span>
                                            
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a href="{{route('users.index')}}" class="nav-link nav-toggle">
                                            <span class="fa fa-list" ></span>
                                            <span class="title">List</span>
                                        </a>
                                       
                                    </li>
                                </ul>
                            </li>


                            
                            
                </ul>
                        