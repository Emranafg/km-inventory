
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Employee Enrty Form</span>
            <span class="caption-helper">Please Add New Employee if He / She is not Existed in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        @if (Session::has('addEmployee'))
            <div class="alert alert-success" role="alert">
                {{Session::get('addEmployee')}}
            </div>            
        @endif
        <form action="{{route('saveEmployee.store')}}" method="POST" class="form-horizontal">
            @csrf
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="name" value="" id="name" placeholder="Enter Name of Employee">
                        @error('name')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Last name</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="last_name" value="" id="lastName" placeholder="Enter Name of Employee">
                        @error('lastName')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Father Name</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="fatherName" value="" id="fatherName" placeholder="Enter Name of Employee">
                        @error('fatherName')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Position</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="position" value="" id="position" placeholder="Enter Name of Employee">
                        @error('position')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Department</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="department_id">
                            @foreach ($deparment as $deparments)
                            <option value="{{$deparments ->id}}">
                                {{$deparments->name}}
                            </option>
                            @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Sub-Department</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="sub_department" value="" id="position" placeholder="Enter sub_department">
                        @error('position')
                        <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Hire Status</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1">
                            <option value=""></option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                            
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
                
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection