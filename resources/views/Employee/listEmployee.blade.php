
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))

@section('body')

      <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase">List of Items</span>
                    </div>
                    <div class="tools">
                        <div class="portlet-title">
                            <div class="actions">
                                <div class="btn-group">
                                    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"> Tools </span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="icon-printer"></i> Print</a>
                                        </li>
                
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="icon-doc"></i> PDF</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="icon-paper-clip"></i> Excel</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="4" class="tool-action">
                                                <i class="icon-cloud-upload"></i> CSV</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="portlet-body">
                    @if (Session::has('deleteEmployee'))
                        <div class="alert alert-danger" role="alert">
                            {{Session::get('deleteEmployee')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Last Name</th>
                                <th>Father Name</th>
                                <th>Posistion</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($employee as $employees )
                            <tr>
                                <td>{{$employees->id}}</td>
                                <td>{{$employees->name}}</td>
                                <td>{{$employees->last_name}}</td>
                                <td>{{$employees->father_name}}</td>
                                <td>{{$employees->sub_department}}</td>
                               
                                <td>
                                    <a href="/editEmployee/{{$employees->id}}" class="btn btn-primary">Edit</a>
                                    <a href="" class="btn btn-success">Show</a>
                                    <a href="/deleteEmployee/{{$employees->id}}" class="btn btn-danger">Delete</a>
                                    
                                </td>
                               <td>

                               </td>
                            </tr>
                            @endforeach
                            
                                         
                          </tbody>
                    </table>
                </div>
            </div>  
            
            
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <!-- END THEME LAYOUT SCRIPTS -->
        <!-- Google Tag Manager -->
    <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-NGFVTN"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '../../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NGFVTN');</script>   
@endsection