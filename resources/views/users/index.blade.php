
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))

@section('body')

      <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase">List of Units</span>
                    </div>
                    <div class="tools">
                        <div class="portlet-title">
                            <div class="actions">
                                <div class="btn-group">
                                    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"> Tools </span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="icon-printer"></i> Print</a>
                                        </li>
                
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="icon-doc"></i> PDF</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="icon-paper-clip"></i> Excel</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="4" class="tool-action">
                                                <i class="icon-cloud-upload"></i> CSV</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="portlet-body">
                    @if (Session::has('delete_unit'))
                        <div class="alert alert-danger" role="alert">
                            {{Session::get('delete_unit')}}
                        </div>
                        
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                              
                            <th>  {{__('general.Username')}}</th>
                        <th>  {{__('general.email')}} </th>
                        <th>  {{__('general.role_mng')}}</th>
                        <th>  {{__('general.Status')}}</th>
                        <th>  {{__('general.Position')}}</th>
                        <th>  {{__('general.action')}}</th>

                            </tr>
                        </thead>
                  
                        <tbody>
                            @foreach ($users as $user )
                            <tr>
                             
                            <td>{{$user->username;}}</td>
                        <td><a href="#">{{$user->email;}}</a></td>
                        <td>  @foreach($user->getRoleNames() as $role)
                            {{ $role }}
                          @endforeach</td>
                        <td>@if ($user->status == 1)
                            <span class="badge badge-success">Active</span>
                       
                                
                            @else
                            <span class="badge badge-success bg-danger">inActive</span>  
                            @endif
                        </td>
                        <td>{{$user->posation;}}</td>
                                <td>
                            <div class="list-icons">
                              
                            <a class="btn btn-info" href="{{ route('users.edit', $user->id)}}" class="list-icons-item text-primary-600" style="margin-right:10px;">edit</i></a>
                              
                        
    
                                <form id="delete{{$user->id}}" action="{{ route('users.destroy', $user->id)}}" method="post">
                                    <button style="float:left" class="btn btn-danger">delete</button>
                                  @csrf
                                  @method('DELETE')
                                </form>
                            </div>
                        </td>
                               <td>

                               </td>
                            </tr>
                            @endforeach
                            
                                         
                          </tbody>
                    </table>
                </div>
            </div>  
            
            
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <!-- END THEME LAYOUT SCRIPTS -->
        <!-- Google Tag Manager -->
    <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-NGFVTN"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '../../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NGFVTN');</script>   
@endsection