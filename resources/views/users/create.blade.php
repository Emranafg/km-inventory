
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Adding New User</span>
            <span class="caption-helper"></span>
        </div>
    
    </div>
    <div class="portlet-body form">
        @if (Session::has('addItem'))
            <div class="alert alert-success" role="alert">
                {{Session::get('addItem')}}
            </div>
            
        @endif
        <!-- BEGIN FORM-->
        <form action="{{route('users.store')}}" method="POST" class="form-horizontal">
            @csrf
            <div class="form-body">
            <div class="form-group">
                    <label class="col-md-2 control-label">Username</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="username" value="" id="item_name" placeholder="Username ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">First Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="fname" value="" id="item_name" placeholder="First Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Last Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="lname" value="" id="item_name" placeholder="Last Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> Email</label>
                    <div class="col-md-8">
                        <input  type="email" class="form-control" name="email" value="" id="item_name" placeholder="Email Adress">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> Contact Number</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="contact_number" value="" id="item_name" placeholder="Contact Number ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> Posation</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="posation" value="" id="item_name" placeholder="Enter Item Name ">
                    </div>
                </div>
    

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Roles</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="role">
                        @foreach ($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option> 
                                    @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
           

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection