@extends('Layout.index')


@section('content')
<!-- Content area -->


<div class="content d-flex justify-content-center align-items-center">
   

    <!-- Registration form -->
    <form action="{{route('profileupdate')}}" class="flex-fill" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-12">
               
                <div class="card mb-0">
                    {{$user->id}}
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">Create account</h5>
                            <span class="d-block text-muted">All fields are required</span>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" placeholder="Choose username" name="username" value="{{$user->username}}" disabled>
                            <div class="form-control-feedback">
                                <i class="icon-user-plus text-muted"></i>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="First name" name="fname" value="{{$user->fname}}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="Second name" name="lname" value="{{$user->lname}}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                      

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="email" class="form-control" placeholder="Your email" name="email" value="{{$user->email}}">
                                    <div class="form-control-feedback">
                                        <i class="icon-mention text-muted"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">+93</span>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" value="{{$user->contact_number}}">
                                    </div>
                                
                                    
                                    <div class="form-control-feedback">
                                        <i class="icon-phone2 text-muted"></i>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" placeholder="job title/posation" name="posation" value="{{$user->posation}}">
                            <div class="form-control-feedback">
                                <i class="icon-user-plus text-muted"></i>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label font-weight-semibold">Single file upload:</label>
                            <div class="col-lg-10">
                                <input type="file" class="file-input form-control-lg" name="image" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
										
                                <span class="form-text text-muted">Uploaded pic will be shown here.</span>
                            </div>
                        </div>
                     

                        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> Update Settings </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- /registration form -->

</div>
<!-- /content area -->

@endsection