
@extends('layouts.app')

@section('page_title', __('general.dashboard'))
@section('title', __('general.dashboard'))



@section('body')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-red-sunglo"></i>
            <span class="caption-subject font-red-sunglo bold uppercase">Item Update Form</span>
            <span class="caption-helper">Please Update Item if it is not Correctly inserted in KM|Stock System</span>
        </div>
    
    </div>
    <div class="portlet-body form">
        @if (Session::has('updateItem'))
            <div class="alert alert-success" role="alert">
                {{Session::get('updateItem')}}
            </div>
            
        @endif
        <!-- BEGIN FORM-->
        <form action="{{route('UpdateItem.Update')}}" method="POST" class="form-horizontal">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Name</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="item_name" value="{{$item->item_name}}" id="item_name" placeholder="Enter Item Name ">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Alert Quantity</label>
                    <div class="col-md-8">
                        <input  type="text" class="form-control" name="alert_quantity" value="{{$item->alert_quantity}}" id="alert_quantity" placeholder="Enter Item Alert Quantity ">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Item Code</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="code" value="{{$item->code}}" id="code" placeholder="Enter Item Code">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Related Section</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="related_section">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">04</option>
                            <option value="10">10</option>
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Image</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="image" value="{{$item->image}}" id="image">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Unit</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="unite_id">
                            @foreach ($unit as $units)
                            <option value="{{$units ->id}}">
                                {{$units->name}}
                            </option>
                            @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Category</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="category_id">
                           @foreach ($category as $cates)
                               <option value="{{$cates->id}}">
                                    {{$cates->name}}
                               </option>
                           @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Warehouse Keeper</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="warehouse_keeper_id">
                            @foreach ($warehouseKeeper as $warehouseKeepers)
                                <option value="{{$warehouseKeepers->id}}">
                                    {{$warehouseKeepers->name}}
                                </option>
                            @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Item Type</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="item_type_id">
                            @foreach ($ItemType as $ItemTypes)
                                <Option value="{{$ItemTypes->id}}">
                                    {{$ItemTypes->name}}
                                </Option>
                            @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="form_control_1">Department</label>
                    <div class="col-md-8">
                        <select class="form-control" id="form_control_1" name="department_id">
                            @foreach ($department as $departments)
                                <option value="{{$departments->id}}">
                                    {{$departments->name}}
                                </option>
                            @endforeach
                        </select>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Item Description</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="description" rows="3">{{$item->description}}</textarea>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Update</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>      
@endsection