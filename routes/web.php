<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\DoonarController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemStatusController;
use App\Http\Controllers\ItemTypeController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WarehouseKeeperController;
use Faker\Guesser\Name;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('test');
});


Route::get('/login', function () {
    return view('login'); 
});



Route::post('/login',[\App\Http\Controllers\LoginController::class, 'login'])->name('login');
Route::get('/logout',[\App\Http\Controllers\LoginController::class, 'logout'])->name('logout');
Route::resource('users', UserController::class)->middleware('auth');
//start of Employee Route
Route::get('/addEmployee',[EmployeeController::class,'create'])->name('addEmployee.create');
Route::post('/saveEmployee',[EmployeeController::class,'store'])->name('saveEmployee.store');
Route::get('/showEmployee',[EmployeeController::class,'show'])->name('showEmployee.show');
Route::get('/deleteEmployee/{id}',[EmployeeController::class,'destroy']);
Route::get('/editEmployee/{id}',[EmployeeController::class,'edit']);
Route::post('/UpdateEmployee',[EmployeeController::class,'update'])->name('UpdateEmployee.Update');
//End

//start of Item Route
Route::get('/addItem',[ItemController::class,'create'])->name('addItem.create');
Route::post('/saveItem',[ItemController::class,'store'])->name('saveItem.store');
Route::get('/showItem',[ItemController::class,'show'])->name('showItem.show');
Route::get('/deleteItem/{id}',[ItemController::class,'destroy']);
Route::get('/editItem/{id}',[ItemController::class,'edit']);
Route::post('/UpdateItem',[ItemController::class,'update'])->name('UpdateItem.Update');
//End

//item Status
Route::get('/addItemStatus',[ItemStatusController::class,'create'])->name('addItemStatus.create');
Route::post('/saveItemStatus',[ItemStatusController::class,'store'])->name('saveItemStatus.store');
Route::get('/showItemStatus',[ItemStatusController::class,'show'])->name('showItemStatus.show');
Route::get('/deleteItemStatus/{id}',[ItemStatusController::class,'destroy']);
Route::get('/editItemStatus/{id}',[ItemStatusController::class,'edit']);
Route::post('/UpdateItemStatus',[ItemStatusController::class,'update'])->name('UpdateItemStatus.Update');
//end
//start of Category
Route::get('/addCategory',[CategoryController::class,'create'])->name('addCategory.create');
Route::post('/saveCategory',[CategoryController::class,'store'])->name('saveCategory.store');
Route::get('/show',[CategoryController::class,'show'])->name('listCategory.show');
Route::get('/deleteCategory/{id}',[CategoryController::class,'destroy']);
Route::get('/editCategory/{id}',[CategoryController::class,'edit']);
Route::post('/UpdateCategory',[CategoryController::class,'update'])->name('UpdateCategory.Update');
//end of Category

//Start of Item Unit Route
Route::get('/addUnit',[UnitController::class,'create'])->name('addUnit.create');
Route::post('/saveUnit',[UnitController::class,'store'])->name('saveUnit.store');
Route::get('/showUnit',[UnitController::class,'show'])->name('showUnit.show');
Route::get('/deleteUnit/{id}',[UnitController::class,'destroy']);
Route::get('/editUnit/{id}',[UnitController::class,'edit']);
Route::post('/UpdateUnit',[UnitController::class,'update'])->name('UpdateUnit.Update');
//End

//Item Type
Route::get('/addItemType',[ItemTypeController::class,'index'])->name('itemType.add');
Route::post('/saveitemType',[ItemTypeController::class,'store'])->name('itemType.store');
Route::get('/listItemType',[ItemTypeController::class,'show'])->name('listItemType.show');
Route::get('/delete/{id}',[ItemTypeController::class,'destroy']);
Route::get('/editItemType/{id}',[ItemTypeController::class,'edit']);
Route::post('/UpdateItemType',[ItemTypeController::class,'update'])->name('ItemType.Update');
//end

//start of department route
Route::get('/addDepartment',[DepartmentController::class,'create'])->name('addDepartment.create');
Route::post('/saveDepartment',[DepartmentController::class,'store'])->name('saveDepartment.store');
Route::get('/showDepartmemt',[DepartmentController::class,'show'])->name('showDepartment.show');
Route::get('/deleteDepartment/{id}',[DepartmentController::class,'destroy']);
Route::get('/editDepartment/{id}',[DepartmentController::class,'edit']);
Route::post('/UpdateDepartment',[DepartmentController::class,'update'])->name('UpdateDepartment.Update');
//end

//start of Donor Route
Route::get('/addDonor',[DoonarController::class,'create'])->name('addDonor.create');
Route::post('/saveDonar',[DoonarController::class,'store'])->name('saveDoonar.store');
Route::get('/showDonar',[DoonarController::class,'show'])->name('showDoonar.show');
Route::get('/deleteDonar/{id}',[DoonarController::class,'destroy']);
Route::get('/editIDonor/{id}',[DoonarController::class,'edit']);
Route::post('/UpdateDonor',[DoonarController::class,'update'])->name('UpdateDonor.Update');
//end

// Warehouse keeper 
Route::get('/addWarehouseKeeper',[WarehouseKeeperController::class,'create'])->name('addWarehouseKeeper.create');
Route::post('/saveWarehouseKeeper',[WarehouseKeeperController::class,'store'])->name('saveWarehouseKeeper.store');
Route::get('showWarehouseKeepers',[WarehouseKeeperController::class,'show'])->name('showWarehouseKeepers.show');
Route::get('/deleteWarehouse/{id}',[WarehouseKeeperController::class,'destroy']);
Route::get('/editWarehousKeeper/{id}',[WarehouseKeeperController::class,'edit']);
Route::post('/UpdatWarehousKeeper',[WarehouseKeeperController::class,'update'])->name('UpdatWarehousKeeper.Update');
//end


Route::get('/404', function () {
    return view('404'); 
});