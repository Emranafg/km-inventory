<?php

namespace App\Http\Controllers;

use App\Models\Fece_1;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Fece1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Fece_1 $fece_1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Fece_1 $fece_1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Fece_1 $fece_1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Fece_1 $fece_1)
    {
        //
    }
}
