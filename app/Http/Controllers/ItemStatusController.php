<?php

namespace App\Http\Controllers;

use App\Models\Item_status;
use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $item_status = Item_status::all();
        return view('Item_status.add_Item_Status',compact('item_status'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $item_status = new Item_status();
        $item_status ->name = $request ->name;
        $item_status->save();
        return back()->with('addItemsStatus','Item Status has been added Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Item_status $item_status)
    {
        $item_status = Item_status::all();
        return view('Item_status.list_Item_Status',compact('item_status'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {   
        $item_status = Item_status::find($id);
        return view('Item_status.edit_Item_status',compact('item_status'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Item_status $item_status)
    {
        $item_status = Item_status::find($request->id);
        $item_status->name = $request->name;
        $item_status->save();
        return back()->with('itemStatus','Item status has been successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        Item_status::where('id',$id)->delete();
        return back()->with('itemStatusDelete','Item Status has been deleted Successfully');
    }
}
