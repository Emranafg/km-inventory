<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $category = Category::all();

        return view('Category.listCategory',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category = Category::all();
        return view('Category.addCategory', compact('category'));
    } 

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category -> name = $request->name;
        $category->save();
        return back()->with('itemCategory','Item Category has been added successfully');

    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        $category = Category::all();
         return view('Category.listCategory',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);
        return view('Category.editCategory',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $category = Category::find($request->id);
        $category ->name = $request->name;
        $category->save();
        return back()->with('updateCategory','Category has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
      Category::where('id',$id)->delete();
        // $category = Category::find($id);
        // $category->delete();
        return back()->with('delete_category','Category has been deleted successfully');

    }
}
