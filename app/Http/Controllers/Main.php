<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Property_registerer;
use App\Models\District;

use Illuminate\Support\Facades\DB;

class Main extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $district = District::all();
        $Parcels = Property::count();
        $parcels_dis = DB::table('properties')
        ->select('District', DB::raw('count(*) as total'))
        ->groupBy('District')
        ->get();

        return view('dashboard.index',compact("Parcels","parcels_dis","district"));
    }

    public function report()
    {
       return view('reports.index');
    }
    public function search(Request $request)
    {
        //dd($request->all());
       // dd($request->all());
       // dd($request->all());
        if($request->type == "date") {


            $csd = explode('/', faTOen($request->sd));
        
            $j_y = $csd[0];
            $j_m = $csd[1];
            $j_d = $csd[2];
            $gcsd = jalali_to_gregorian($j_y, $j_m, $j_d);
                    return $gcsd;
            $ced = explode('/', faTOen($request->ed));
            
            $j_y = $ced[0];
            $j_m = $ced[1];
            $j_d = $ced[2];
            $gced = jalali_to_gregorian($j_y, $j_m, $j_d)."";
            $properties = Property::whereBetween('created_at', [$gcsd, $gced])
                    ->get();
                    return $properties;
                   // return $property;
            return view('reports.index', compact("properties"));
            
        }
        else
        {
            $properties = Property::where("name","LIKE","%$request->name%")
            ->where("f_name","LIKE","%$request->father_name%")
            ->where("g_father_name","LIKE","%$request->g_father_name%")
            ->where("District","LIKE","%$request->district%")
            ->where("Guzar","LIKE","%$request->Guzar%")
            ->where("Block","LIKE","%$request->block%")
            ->where("Parcel","LIKE","%$request->parcel%")
            ->where("Unit_nu","LIKE","%$request->unit%")
            // ->where("district",$request->district)
            ->get();
            
            return view('reports.index', compact("properties"));
        }
       return "starting date ".$gcsd."<br> ending date".$gced;
    }
    public function print($id)
    {
        
        $property = Property::find($id);
        $property_reg = Property_registerer::find($property->reg_id);
        
        return view('Property.print',compact("property","property_reg"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
}
