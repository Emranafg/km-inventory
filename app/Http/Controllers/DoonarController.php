<?php

namespace App\Http\Controllers;

use App\Models\Doonar;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DoonarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $doonar = Doonar::all();
        return view('Donor.addDonor',compact('doonar'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $doonar = new Doonar();

        $doonar->name = $request->name;
        $doonar->save();
        return back()->with('add_doonar','Donar has been Added Successfully');


    }

    /**
     * Display the specified resource.
     */
    public function show(Doonar $doonar)
    {
        $doonar = Doonar::all();
        return view('Donor.listDonor', compact('doonar'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $doonar = Doonar::find($id);
        return view('Donor.editDonor',compact('doonar'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Doonar $doonar)
    {
        //
        $doonar = Doonar::find($request->id);
        $doonar ->name = $request->name;
        $doonar->save();
        return back()->with('donorUpdate','Donor has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        Doonar::where('id',$id)->delete();
        return back()->with('delete_Donar','Donar has been deleted successfully');
    }
}
