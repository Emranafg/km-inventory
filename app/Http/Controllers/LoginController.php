<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
     public function login(Request $request)
    {
       
        
        if(Auth::attempt($request->only('email','password'))  && Auth::user()->status == '1') {
           
           $request->session()->put('username', Auth::user()->username);
           $request->session()->put('id', Auth::user()->id);
        
         
           $request->session()->put('name', Auth::user()->fname.' '.Auth::user()->lname);
           $request->session()->put('email', Auth::user()->email);
            
             return redirect('/');
            
        }
        else {

            return redirect()->route('login')->with('message', 'User '.$request->username.' does not exist in the server');


        }
    }
    public function logout()
    {
         $locale = session('locale');
        session()->flush();
       
         
         
        Auth::logout();
        session()->put('locale', $locale);
        return redirect()->route('login');
    }
}
