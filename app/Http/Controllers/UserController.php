<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = User::all();
        return view('users.index',['users' => $all]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $user = new User;
        
        $user->username = $request->username;
        
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        
        $user->email = $request->email;
        
        $user->contact_number = $request->contact_number;
        $user->posation = $request->posation;
        $user->status = "1";
        $user->password = Hash::make('Afghan@786');
        $user->save();
        
        $user->syncRoles($request['role']);
        
        return redirect()->route('users.index')->with('message', 'User added!</span> User Account '.$request->username.' have been Added');
     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return "show <br>".$user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        
        $roles = Role::all();
        $userRole = $user->roles;
        
         return view('users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $user = User::find($id);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->posation = $request->posation;
        $user->status = $request->status;
        $user->save();
        $user->syncRoles($request->role);
        
        return redirect()->route('users.index')->with('message', 'User '.$user->username.'! has been edited ');
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $delete_user = User::find($user->id);
        $delete_user->delete();

        return redirect()->route('users.index')->with('message', 'User Deleted!</span> User Account '.$user->username.' have been deleted');
     
    }
    public function setting()
    {
     
     $user = User::find(session('id'));
     
       return view('users.setting' ,compact('user'));
       
    }
     public function profileupdate(Request $request)
    {
        
    
        if($request->hasFile('image')) {
            $ext = $request->file('image')->getClientOriginalExtension();
            $file = session('username').'.jpg';
           
            $request->file('image')->storeAs('public/Users/',$file );
        }
        $user = User::find(session('id'));
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->posation = $request->posation;
        $user->save();
     


        return redirect()->route('setting')->with('message', 'profile Updated!</span> User Account '.$user->username.' have been Updated');
     

       # $user = User::find(session('id'));
       # return $user;
    }
}
