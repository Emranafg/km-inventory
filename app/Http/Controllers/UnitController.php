<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //

        return view('Unit.add_Unit');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request ->validate([
            'name' => 'required|max:15'
        ]);
        $unit = new Unit();
        $unit ->name = $request ->name;
        $unit ->save();
        return back()->with('add_unit','Item Unit is added Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Unit $unit)
    {
        $unit = Unit::all();
        return view('Unit.list_Unit',compact('unit'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $unit = Unit::find($id);
        return view('Unit.edit_Unit',compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Unit $unit)
    {
        //
        $unit = Unit::find($request->id);
        $unit ->name = $request->name;
        $unit->save();
        return back()->with('upadatUnite','Unit has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Unit::where('id',$id)->delete();
        return back()->with('delete_unit','Item Unit has been deleted successfully');
    }
}
