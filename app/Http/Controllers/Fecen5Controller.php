<?php

namespace App\Http\Controllers;

use App\Models\Fecen_5;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Fecen5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Fecen_5 $fecen_5)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Fecen_5 $fecen_5)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Fecen_5 $fecen_5)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Fecen_5 $fecen_5)
    {
        //
    }
}
