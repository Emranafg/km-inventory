<?php

namespace App\Http\Controllers;

use App\Models\warehouse_keeper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WarehouseKeeperController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $warehouse_keeper = warehouse_keeper::all();
        return view('Warehouse_Keepers.addWarehouseKeepers',compact('warehouse_keeper'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $warehouse_keeper = new warehouse_keeper();
        $warehouse_keeper ->name = $request->name;
        $warehouse_keeper ->last_name = $request->last_name;
        $warehouse_keeper ->phone = $request->phone;
        $warehouse_keeper ->email = $request->email;
        $warehouse_keeper ->save();
        return back()->with('addWarehouseKeeper','Warehouse Keeper has been added Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(warehouse_keeper $warehouse_keeper)
    {
        $warehouse_keeper = warehouse_keeper::all();
        return view('Warehouse_Keepers.listWarehouseKeepers', compact('warehouse_keeper'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $warehouse_keeper = warehouse_keeper::find($id);
        return view('Warehouse_Keepers.editWarehouseKeeper',compact('warehouse_keeper'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, warehouse_keeper $warehouse_keeper)
    {
        //
        $warehouse_keeper = warehouse_keeper::find($request->id);
        $warehouse_keeper ->name = $request->name;
        $warehouse_keeper ->last_name = $request->last_name;
        $warehouse_keeper ->phone = $request->phone;
        $warehouse_keeper ->email = $request->email;
        $warehouse_keeper->save();
        return back()->with('warehouseKeeperUpdate','Warehouse Keeper has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        
        warehouse_keeper::where('id',$id)->delete();
        return back()->with('warehouse_delete','Warehouse Keeper has been deleted Successfully');
    }
}
