<?php

namespace App\Http\Controllers;

use App\Models\Item_Type;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $itemType = Item_Type::all();

        return view('Item_type.add_Item_Type',compact('itemType'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $itemType = Item_Type::all();

        return view('Item_type.add_Item_Type',compact('itemType'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $itemType = new Item_Type();
        $itemType ->name = $request->name;
        $itemType ->save();
        return back()->with('add_Item_Type','Item Type added Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Item_Type $item_Type)
    {
        // 
        $itemType = Item_Type::all();
        return view('Item_type.list_Item_type',compact('itemType'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // $item_Type = Item_Type::find($id);
        // return view('Item_type.edit_Item_Type',compact('item_Type'));
        $itemType = Item_Type::find($id);
        return view('Item_type.edit_Item_Type',compact('itemType'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {   
        $item_Type = Item_Type::find($request->id);
        $item_Type ->name = $request->name;
        $item_Type->save();
        return back()->with('itemTypeUpdate','Item Type has been Updated Successfully');
        // $item_Type = Item_Type::find($request->id);
        // $item_Type ->name = $request->name;
        // $item_Type->save;
        // return back()->with('itemTypeUpdated','Item Type has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        Item_Type::where('id',$id)->delete();
        return back()->with('item_type_delete','Item Type has been deleted successfully');
    }
}
