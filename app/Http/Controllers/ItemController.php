<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\department;
use App\Models\Item_Type;
use App\Models\Unit;
use App\Models\warehouse_keeper;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $unit = Unit::all();
        $category = Category::all();
        $warehouseKeeper = warehouse_keeper::all();
        $ItemType = Item_Type::all();
        $department = department::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $unit = Unit::all();
        $category = Category::all();
        $warehouseKeeper = warehouse_keeper::all();
        $ItemType = Item_Type::all();
        $department = department::all();
        return view('Item.addItem',compact('unit','category','warehouseKeeper','ItemType','department'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $item = new Item();
        $item ->item_name = $request->item_name;
        $item ->alert_quantity = $request->alert_quantity;
        $item ->code = $request->code;
        $item ->related_section = $request->related_section;
        $item ->image = $request->image;
        $item ->unite_id = $request->unite_id;
        $item ->category_id = $request->category_id;
        $item ->warehouse_keeper_id = $request->warehouse_keeper_id;
        $item ->item_type_id = $request->item_type_id;
        $item ->department_id = $request->department_id;
        $item ->description = $request->description;
        $item->save();
        return back()->with('addItem','Item has been added Successfully');


        
    }

    /**
     * Display the specified resource.
     */
    public function show(Item $item)
    {
        $unit = Unit::all();
        $category = Category::all();
        $warehouseKeeper = warehouse_keeper::all();
        $ItemType = Item_Type::all();
        $department = department::all();
        $item = Item::all();
        return view('Item.list_Item',compact('item','category','warehouseKeeper','ItemType','department'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $unit = Unit::all();
        $category = Category::all();
        $warehouseKeeper = warehouse_keeper::all();
        $ItemType = Item_Type::all();
        $department = department::all();   
        $item = Item::find($id);
        return view('Item.Edit_Item',compact('item','category','warehouseKeeper','ItemType','department','unit'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Item $item)
    {
        $item = Item::find($request->id);
        $item ->item_name = $request->item_name;
        $item ->alert_quantity = $request->alert_quantity;
        $item ->code = $request->code;
        $item ->related_section = $request->related_section;
        $item ->image = $request->image;
        $item ->unite_id = $request->unite_id;
        $item ->category_id = $request->category_id;
        $item ->warehouse_keeper_id = $request->warehouse_keeper_id;
        $item ->item_type_id = $request->item_type_id;
        $item ->department_id = $request->department_id;
        $item ->description = $request->description;
        $item ->save();
        return back()->with('updateItem','Item has been update successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Item::where('id',$id)->delete();
        return back()->with('deleteItem','Item has been deleted successfully');  
    }
}
