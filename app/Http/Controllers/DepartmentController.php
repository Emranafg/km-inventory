<?php

namespace App\Http\Controllers;

use App\Models\department;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $diretorate = department::all();
        return view('Directorate.addDirectorate',compact('diretorate'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $department = new department();
        $department ->name = $request->name;
        $department->save();
        return back()->with('add_dep','Department has been added Successfull');
    }

    /**
     * Display the specified resource.
     */
    public function show(department $department)
    {
        $department = department::all();
        return view('Directorate.listDirectorate',compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $department = department::find($id);
        return view('Directorate.editDirectorate',compact('department'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, department $department)
    {
        $department = department::find($request->id);
        $department ->name = $request->name;
        $department->save();
        return back()->with('updateDepartment','Department has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        department::where('id',$id)->delete();
        return back()->with('delete_department','Department has been deleted Successfullly');
    }
}
