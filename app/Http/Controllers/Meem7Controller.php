<?php

namespace App\Http\Controllers;

use App\Models\Meem_7;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Meem7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Meem_7 $meem_7)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Meem_7 $meem_7)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Meem_7 $meem_7)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Meem_7 $meem_7)
    {
        //
    }
}
