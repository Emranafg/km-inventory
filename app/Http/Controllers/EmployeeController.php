<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Http\Controllers\Controller;
use App\Models\department;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $deparment = department::all();
        $employee = Employee::all();
        return view('Employee.addEmployee',compact('employee','deparment'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $deparment = department::all();
        $validateData = $request->validate([

        ]);
        
        $employee = new Employee();
        $employee ->name = $request->name;
        $employee -> last_name = $request->last_name;
        $employee -> father_name = $request->fatherName;
        $employee -> department_id = $request->department_id;
        $employee -> position = $request->position;
        $employee -> department = $request->department;
        $employee -> sub_department = $request->sub_department;
        //$employee -> hire_status = $request->hire_status;
        $employee->save();
        return back()->with('addEmployee','Employee has been added successfully',compact('deparment'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Employee $employee)
    {
        //
        $employee = Employee::all();
        return view('Employee.listEmployee',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $deparment = department::all();
        $employee = Employee::find($id);
        return view('Employee.editEmployee',compact('employee','deparment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Employee $employee)
    {
        $employee = Employee::find($request->id);
        $employee ->name = $request->name;
        $employee ->last_name = $request->last_name;
        $employee ->fatherName = $request->fatherName;
        $employee ->position = $request->position;
        $employee ->sub_department = $request->sub_department;
        $employee ->name = $request->name;
        $employee ->save();

        return back()->with('updateEmployee','Employee has been updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Employee::where('id',$id)->delete();
        return back()->with('deleteEmployee','Employee has been deleted successfully');
        
    }
}
