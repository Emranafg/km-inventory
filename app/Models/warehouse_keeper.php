<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class warehouse_keeper extends Model
{
    use HasFactory;
    public function item() {
        return $this->hasMany(item::class,'warehouse_keeper_id'); 
    }
    public function Meem_7_Details() { 
        return $this->hasMany(Meem_7_Details::class,'warehouse_keeper_id'); 
    }
    public function Fecen_5() {
        return $this->hasMany(Fecen_5::class,'warehouse_keeper_id'); 
    } 
    public function fece_1() { 
        return $this->hasMany(fece_1::class,'warehouse_keeper_id'); 
    }
    public function fecen_8() {
        return $this->hasMany(fecen_8::class,'warehouse_keeper_id'); 
    }
}
