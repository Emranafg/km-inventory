<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fece_1 extends Model
{
    use HasFactory;
    public function item() {
        return $this->belongsTo(item::class);
    }
    public function Item_status() {
        return $this->belongsTo(Item_status::class);
    }
    public function warehouse_keeper() {
        return $this->belongsTo(warehouse_keeper::class);
    }
}
