<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory; 
    public function Unit() {
        return $this->belongsTo(Unit::class); 
    }
    public function Category() {
        return $this->belongsTo(Category::class);
    }
    public function warehouse_keeper() {
        return $this->belongsTo(warehouse_keeper::class);
    }
    public function Item_Type() { 
        return $this->belongsTo(Item_Type::class);
    }
    public function department() {
        return $this->belongsTo(department::class);
    }
    public function Meem_7_Details() {
        return $this->hasMany(Meem_7_Details::class,'item_id'); 
    }
    public function Fecen_5() { 
        return $this->hasMany(Fecen_5::class,'item_id'); 
    } 
    public function fece_1() {
        return $this->hasMany(fece_1::class,'item_id'); 
    } 
    public function fecen_9() {
        return $this->hasMany(fecen_9::class,'item_id'); 
    } 
    public function fecen_8() {
        return $this->hasMany(fecen_8::class,'item_id'); 
    }
    public function storage__card() {
        return $this->hasMany(storage__card::class,'item_id'); 
    } 
}
