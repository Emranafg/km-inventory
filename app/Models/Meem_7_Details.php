<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meem_7_Details extends Model
{
    use HasFactory;
    public function Item() {
        return $this->belongsTo(Item::class);
    }
    public function Meem_7() { 
        return $this->belongsTo(Meem_7::class);
    }
    public function department() { 
        return $this->belongsTo(department::class);
    }
    public function warehouse_keeper() { 
        return $this->belongsTo(warehouse_keeper::class);
    }
}
