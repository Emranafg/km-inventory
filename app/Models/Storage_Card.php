<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Storage_Card extends Model
{
    use HasFactory;
    public function item() {
        return $this->belongsTo(item::class);
    }
    public function item_statuse() {
        return $this->belongsTo(item_statuse::class);
    }
    public function employee() {
        return $this->belongsTo(employee::class);
    }
}
