<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fecen_9 extends Model
{
    use HasFactory; 
    public function employee() {
        return $this->belongsTo(employee::class);
    }
    public function item() {
        return $this->belongsTo(item::class);
    }
    public function Fecen_5() { 
        return $this->hasMany(Fecen_5::class,'Fecen_9_id'); 
    }
}
