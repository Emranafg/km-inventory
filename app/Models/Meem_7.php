<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meem_7 extends Model
{
    use HasFactory;
    public function Meem_7_Details() { 
        return $this->hasMany(Meem_7_Details::class,'meem_7_id'); 
    }
    public function Fecen_5() {
        return $this->hasMany(Fecen_5::class,'meem_7_id'); 
    }    public function doonar() {
        return $this->belongsTo(doonar::class);
    }
}
