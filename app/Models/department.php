<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    use HasFactory;
    public function item() {
        return $this->hasMany(item::class,'department_id'); 
    }
    public function Meem_7_Details() { 
        return $this->hasMany(Meem_7_Details::class,'department_id'); 
    }
    public function Employee() { 
        return $this->hasMany(Employee::class,'department_id'); 
    }
}
