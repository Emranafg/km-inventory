<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item_status extends Model
{
    use HasFactory; 
    public function storage__card() {
        return $this->hasMany(storage__card::class,'item_status_id'); 
    }
    public function Fece_1() {
        return $this->hasMany(Fece_1::class,'item_status_id'); 
    } 
    
}
