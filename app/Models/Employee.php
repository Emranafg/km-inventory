<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory; 
    public function fecen_9() {  
        return $this->hasMany(fecen_9::class,'emp_id'); 
    }
    public function department() {
        return $this->belongsTo(department::class);
    } 
    public function fecen_8() {
        return $this->hasMany(fecen_8::class,'return_emp_id'); 
    }
    public function storage__card() {
        return $this->hasMany(storage__card::class,'reciver_emp_id'); 
    }
}
