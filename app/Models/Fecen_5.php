<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fecen_5 extends Model
{
    use HasFactory;
    public function item() {
        return $this->belongsTo(item::class);
    }
    public function meem_7() {
        return $this->belongsTo(meem_7::class);
    }
    public function warehouse_keeper() {
        return $this->belongsTo(warehouse_keeper::class);
    }
    public function fecen_9() {
        return $this->belongsTo(fecen_9::class);
    }
    
}
