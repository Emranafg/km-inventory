<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fecen_5s', function (Blueprint $table) {
            $table->id();
            $table->integer('fece_5_no');
            $table->date('issue_date');
            $table->integer('item_quantity');
            $table->string('file');
            $table->integer('fece_9_id');
            $table->integer('item_id');
            $table->integer('meem_7_id');
            $table->integer('warehouse_keeper_id');
            $table->integer('description');
            $table->timestamps();
            // $table->foreign('fece_9_id')->references('id')->on('fece_9s');
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('meem_7_id')->references('id')->on('meem_7s');
            // $table->foreign('warehouse_keeper_id')->references('id')->on('warehouse_keepers');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fecen_5s');
    }
};
