<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fecen_9s', function (Blueprint $table) {
            $table->id();
            $table->integer('fece_9_no');
            $table->integer('fece_9_date');
            $table->integer('item_quantity');
            $table->string('description');
            $table->string('file');
            $table->integer('emp_id');
            $table->integer('item_id');
            $table->timestamps();
            // $table->foreign('emp_id')->references('id')->on('employees');
            // $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fecen_9s');
    }
};
