<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meem_7__details', function (Blueprint $table) {
            $table->id();
            $table->integer('item_quantity');
            $table->integer('item_unit_price');
            $table->integer('meem_7_id');
            $table->integer('item_id');
            $table->integer('department_id');
            $table->integer('warehouse_keeper_id');
            $table->timestamps();
            // $table->foreign('meem_7_id')->references('id')->on('meem_7s');
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('department_id')->references('id')->on('departments');
            // $table->foreign('warehouse_keeper_id')->references('id')->on('warehouse_keepers');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meem_7__details');
    }
};
