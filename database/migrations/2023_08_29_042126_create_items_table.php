<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('item_name');
            $table->integer('alert_quantity');
            $table->integer('quantity');
            $table->integer('code');
            $table->integer('related_section');
            $table->integer('unite_price');
            $table->string('image');
            $table->integer('unite_id');
            $table->integer('category_id');
            $table->integer('warehouse_keeper_id');
            $table->integer('item_type_id');
            $table->integer('department_id');
            $table->string('description');
            $table->timestamps();
            // $table->foreign('unite_id')->references('id')->on('units');
            // $table->foreign('category_id')->references('id')->on('categories');
            // $table->foreign('warehouse_keeper_id')->references('id')->on('warehouse_keepers');
            // $table->foreign('item_type_id')->references('id')->on('item__types');
            // $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
