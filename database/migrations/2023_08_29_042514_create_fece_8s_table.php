<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fece_8s', function (Blueprint $table) {
            $table->id();
            $table->integer('fece_8_no');
            $table->integer('fece_8_date');
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('stats');
            $table->string('description');
            $table->integer('item_id');
            $table->integer('warehouse_keeper_id');
            $table->integer('return_emp_id');
            $table->timestamps();
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('warehouse_keeper_id')->references('id')->on('warehouse_keepers');
            // $table->foreign('return_emp_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fece_8s');
    }
};
