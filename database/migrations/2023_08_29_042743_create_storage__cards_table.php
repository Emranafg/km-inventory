<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('storage__cards', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity');
            $table->integer('item_id');
            $table->integer('item_status_id');
            $table->integer('reciver_emp_id');
            $table->string('discription');
            $table->timestamps();
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('item_status_id')->references('id')->on('item_statuses');
            // $table->foreign('reciver_emp_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('storage__cards');
    }
};
