<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fece_1s', function (Blueprint $table) {
            $table->id();
            $table->integer('fece_1_ref_no');
            $table->integer('fece_1_date');
            $table->integer('price');
            $table->integer('item_id');
            $table->integer('item_status_id');
            $table->integer('warehouse_keeper_id');
            $table->timestamps();
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('item_status_id')->references('id')->on('departments');
            // $table->foreign('warehouse_keeper_id')->references('id')->on('warehouse_keepers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fece_1s');
    }
};
