<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meem_7s', function (Blueprint $table) {
            $table->id();
            $table->integer('meem_7_no');
            $table->date('meem_7_date');
            $table->string('procuremetn_maktoob_no');
            $table->date('procuremetn_date');
            $table->string('rfq_no');
            $table->integer('donar_id'); 
            $table->string('file');
            $table->string('description');
            $table->timestamps();
            //$table->foreign('donar_id')->references('id')->on('doonars');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meem_7s');
    }
};
