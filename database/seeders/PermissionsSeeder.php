<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        Permission::create(['name' => 'Dashboard']);
        Permission::create(['name' => 'User Management']);
        Permission::create(['name' => 'Wakil']);
        Permission::create(['name' => 'Report']);
        Permission::create(['name' => 'Role Management']);

        Permission::create(['name' => 'User Edit']);
        Permission::create(['name' => 'User View']);
        Permission::create(['name' => 'User Delete']);
        Permission::create(['name' => 'Wakil Edit']);
        Permission::create(['name' => 'Wakil View']);
        Permission::create(['name' => 'Wakil Delete']);
       
        
    }
}
