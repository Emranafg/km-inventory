<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\District;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(
            [
                'fname' => 'Emran',
                'lname' => 'Nekzad',
                'username' => 'admin',
                'email' => 'afg.emr@gmail.com',
                'contact_number' => '781567567',
                'status' => '1',
                'posation' => 'system admin',
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
               
            ]
        );
        // District::create(['district_no' => '01','no_guzar' => '7']);
        // District::create(['district_no' => '02','no_guzar' => '8']);
        // District::create(['district_no' => '03','no_guzar' => '12']);
        // District::create(['district_no' => '04','no_guzar' => '21']);
        // District::create(['district_no' => '05','no_guzar' => '39']);
        // District::create(['district_no' => '06','no_guzar' => '36']);
        // District::create(['district_no' => '07','no_guzar' => '40']);
        // District::create(['district_no' => '08','no_guzar' => '50']);
        // District::create(['district_no' => '09','no_guzar' => '27']);
        // District::create(['district_no' => '10','no_guzar' => '25']);
        // District::create(['district_no' => '11','no_guzar' => '28']);
        // District::create(['district_no' => '12','no_guzar' => '36']);
        // District::create(['district_no' => '13','no_guzar' => '63']);
        // District::create(['district_no' => '14','no_guzar' => '23']);
        // District::create(['district_no' => '15','no_guzar' => '32']);
        // District::create(['district_no' => '16','no_guzar' => '27']);
        // District::create(['district_no' => '17','no_guzar' => '50']);
        // District::create(['district_no' => '18','no_guzar' => '11']);
        // District::create(['district_no' => '19','no_guzar' => '14']);
        // District::create(['district_no' => '20','no_guzar' => '20']);
        // District::create(['district_no' => '21','no_guzar' => '19']);
        // District::create(['district_no' => '22','no_guzar' => '19']);
        
        $user->assignRole('admin');

    }
}
